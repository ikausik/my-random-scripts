import requests
import m3u8
from decouple import config
# import subprocess

url = config("VIDEO_M3U8_URL")

r = requests.get(url)

m3u8_master = m3u8.loads(r.text)
type(m3u8_master)

playlist_url = m3u8_master.data['playlists'][3]['uri']
r = requests.get(playlist_url)
playlist = m3u8.loads(r.text)

r = requests.get(playlist.data['segments'][0]['uri'])
with open("video1.ts", "wb") as f:
    for segment in playlist.data["segments"]:
        url = segment["uri"]
        r = requests.get(url)
        f.write(r.content)

# subprocess.run(["ffmpeg", "-i", "vid1.ts", "vid1.mp4"])

